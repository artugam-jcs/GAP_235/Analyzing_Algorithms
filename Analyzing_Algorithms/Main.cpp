#include <iostream>

void Swap(int arr[], int i, int j)
{
	int tmp = arr[i];
	arr[i] = arr[j];
	arr[j] = tmp;
}

void Sort(int arr[], int size)
{
	int n = 0;

	for (int i = 0; i < size; ++i)
	{
		int smallest = arr[i];
		int smallestJ = i;

		for (int j = i; j < size; ++j)
		{
			if (smallest > arr[j])
			{
				smallest = arr[j];
				smallestJ = j;
			}

			++n;
		}

		Swap(arr, i, smallestJ);

		++n;
	}

	std::cout << "n: " << n << std::endl;
}

int main()
{
	int arr[] = { 8, 5, 3, 9, 4, 1, 6, 2, 7 };
	//int arr[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
	int size = 9;

	Sort(arr, size);

	for (int i = 0; i < size; ++i)
	{
		std::cout << arr[i] << " ";
	}

	return 0;
}
